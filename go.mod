module ahmee-controller

go 1.16

require (
	github.com/aws/aws-sdk-go v1.36.32
	github.com/hashicorp/logutils v1.0.0
	github.com/imdario/mergo v0.3.11 // indirect
	golang.org/x/time v0.0.0-20201208040808-7e3f01d25324 // indirect
	gotest.tools v2.2.0+incompatible
	k8s.io/api v0.18.15
	k8s.io/apimachinery v0.18.15
	k8s.io/client-go v0.18.15
	k8s.io/utils v0.0.0-20210111153108-fddb29f9d009 // indirect
)
