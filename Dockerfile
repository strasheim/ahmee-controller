FROM registry.gitlab.com/strasheim/go-container AS GOLANG

WORKDIR /go/working
COPY go.* /go/working/
COPY *.go /go/working/
COPY .golangci.yml . 
ENV GOARCH=amd64 GOOS=linux CGO_ENABLED=0
RUN golangci-lint run
RUN go test
RUN go build -ldflags "-s -w -extldflags \"-static\""


FROM scratch 
COPY --from=golang /go/working/ahmee-controller /ahmee-controller
COPY --from=golang /etc/ssl/certs/* /etc/ssl/certs/
USER 101
ENTRYPOINT ["/ahmee-controller"]
