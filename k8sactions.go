package main

import (
	"context"
	"encoding/json"
	"log"
	"os"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	corev1 "k8s.io/api/core/v1"
	policy "k8s.io/api/policy/v1beta1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/informers"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/tools/clientcmd"
)

const baseimage = "Amazon Linux 2"
const containerName = "ahmee-controller"
const gracetime = 45
const drainWait = 15 * time.Second
const drainMaxWait = 240 * time.Second
const envtrue = "true"

var singleClient *kubernetes.Clientset //nolint -- easy global struct

type worker struct {
	items []eksglue
}

type eksglue struct {
	instanceID string
	nodeName   string
}

// deletes an element from the worker list
func (data *worker) delete(instanceID string) {
	for i := range data.items {
		if data.items[i].instanceID == instanceID {
			data.items = append(data.items[:i], data.items[i+1:]...)
			return
		}
	}
}

// awsslice creates a list directly useable by AWS SDK API calls
func (data worker) awsslice() []*string {
	instances := make([]*string, 0, len(data.items))
	for _, glue := range data.items {
		instances = append(instances, aws.String(glue.instanceID))
	}
	return instances
}

// k8sslice creates a list of k8s node names
func (data worker) k8sslice() []string {
	nodes := make([]string, 0, len(data.items))
	for _, glue := range data.items {
		nodes = append(nodes, glue.nodeName)
	}
	return nodes
}

// mapping maps ether k8s to aws or aws to k8s node/instance names
func (data worker) mapping(in string) string {
	for _, v := range data.items {
		if v.instanceID == in {
			return v.nodeName
		}
		if v.nodeName == in {
			return v.instanceID
		}
	}
	return "unknown input"
}

// find instance ID known to the cluster -- label on each node
// further limiting this only Amazon Linux 2 nodes, that are spawned by eksctl
func findInstanceInK8s() worker {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	listOptions := metav1.ListOptions{}
	k8s := k8sclient()

	nodes, err := k8s.CoreV1().Nodes().List(ctx, listOptions)
	must(err)

	var glue worker
	var myhost eksglue
	for i := range nodes.Items {
		instance := nodes.Items[i].Labels["alpha.eksctl.io/instance-id"]
		if nodes.Items[i].Status.NodeInfo.OSImage != baseimage {
			continue
		}
		if nodes.Items[i].Labels[containerName] == "ignore" {
			log.Println("[INFO]", "ignoring", instance)
			continue
		}
		if nodes.Items[i].Name == os.Getenv("instancename") {
			log.Println("[DEBUG]", "found my own instance", nodes.Items[i].Name)
			myhost = eksglue{instance, nodes.Items[i].Name}
			continue
		}
		log.Println("[DEBUG] found instance", instance, nodes.Items[i].Name)
		glue.items = append(glue.items, eksglue{instance, nodes.Items[i].Name})
	}
	// adding myhost as last item to the list of hosts
	if myhost.instanceID != "" {
		glue.items = append(glue.items, myhost)
	}
	log.Println("[DEBUG] found", len(glue.items), "in the current k8s cluster without ignore label")
	finalGlue := onlyOldInstances(glue)
	log.Println("[DEBUG] found", len(finalGlue.items), "in the current k8s cluster that need upgrading")
	return finalGlue
}

// For aws ahmee selection we require to know the cluster version
func getCurrentK8sVersion() string {
	k8s := k8sclient()
	version, err := k8s.ServerVersion()
	must(err)
	highLevel := version.Major + "." + version.Minor
	highLevel = strings.ReplaceAll(highLevel, "+", "")
	log.Println("[DEBUG]", "current cluster version", highLevel)
	return highLevel
}

// per new nodes you will receive tick from the channel
// called as go tickPerReadyNode and loop over tick
func tickPerReadyNode(tick, done chan bool, existingNodes []string) {
	factory := informers.NewSharedInformerFactory(k8sclient(), 0)
	informer := factory.Core().V1().Nodes().Informer()
	stopper := make(chan struct{})
	defer close(tick)
	defer close(stopper)
	informer.AddEventHandler(cache.ResourceEventHandlerFuncs{
		UpdateFunc: func(oldobj interface{}, newobj interface{}) {
			node := newobj.(*corev1.Node)

			if inSlice(existingNodes, node.GetName()) {
				log.Println("[DEBUG]", "event is for already known node", node.GetName())
				return
			}

			for _, condition := range node.Status.Conditions {
				if condition.Status == "True" && condition.Type == "Ready" {
					existingNodes = append(existingNodes, node.GetName())
					log.Println("[INFO]", "node", node.Name, "became ready - sending tick")
					tick <- true
				}
			}
		},
	})
	go informer.Run(stopper)
	<-done
}

// detachDrainTerminateNodes is a loop around detach node, drain node and terminate a node
// If nodegroups are per AZ this should not get stuck
func detachDrainTerminateNodes(term chan bool, eksworker worker) {
	readyTicker := make(chan bool, 1)
	done := make(chan bool, 1)
	defer close(done)
	log.Println("[INFO]", "detach drain cycle for", len(eksworker.k8sslice()), "nodes")
	go tickPerReadyNode(readyTicker, done, eksworker.k8sslice())
	for current, node := range eksworker.k8sslice() {
		err := detachInstance(eksworker.mapping(node))
		if err != nil {
			if err.Error() == "tick needed" {
				log.Println("[INFO]", "node", node, "was already detached - sending tick")
				readyTicker <- true
			} else {
				must(err)
			}
		}
		<-readyTicker
		drainNode(node)
		log.Println("[INFO]", "node", node, eksworker.mapping(node), "is going to be terminated", "[", current+1, "/", len(eksworker.k8sslice()), "]")
		err = terminateInstance(eksworker.mapping(node))
		if err != nil {
			log.Println("[ERROR]", err)
		}
	}
	term <- true
}

// to allow the draining to be re-entered, which would need to happen in the same sequence
// This singled out of the function above
func drainNode(node string) {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()
	k8s := k8sclient()

	payload := []patchBoolValue{{
		Op:    "replace",
		Path:  "/spec/unschedulable",
		Value: true,
	}}
	payloadBytes, err := json.Marshal(payload)
	must(err)
	_, err = k8s.CoreV1().Nodes().Patch(ctx, node, types.JSONPatchType, payloadBytes, metav1.PatchOptions{})
	must(err)
	log.Println("[INFO]", "node", node, "set Unschedulable to: true")

	var podsWithPolicy []*policy.Eviction
	pods, err := k8s.CoreV1().Pods("").List(ctx, metav1.ListOptions{})
	must(err)
	for i := range pods.Items {
		if !(pods.Items[i].Spec.NodeName == node && !(strings.HasPrefix(pods.Items[i].Name, containerName) && pods.Items[i].Namespace == os.Getenv("namespace"))) {
			continue // if we are on the wrong node or it the ahmee controller itself we skip the action
		}
		if pods.Items[i].ObjectMeta.OwnerReferences[0].Kind == "DaemonSet" {
			continue // daemonset don't need deletion and it causes issues if deleted too early, as the kube-proxy is a daemonset as well
		}
		log.Println("[INFO]", "pod to delete", pods.Items[i].ObjectMeta.Name, "in", pods.Items[i].ObjectMeta.Namespace, "on node", pods.Items[i].Spec.NodeName)
		gracePeriodSeconds := int64(gracetime)
		eviction := &policy.Eviction{
			TypeMeta: metav1.TypeMeta{},
			ObjectMeta: metav1.ObjectMeta{
				Name:      pods.Items[i].ObjectMeta.Name,
				Namespace: pods.Items[i].ObjectMeta.Namespace,
			},
			DeleteOptions: &metav1.DeleteOptions{
				GracePeriodSeconds: &gracePeriodSeconds,
			},
		}
		err = k8s.PolicyV1beta1().Evictions(pods.Items[i].ObjectMeta.Namespace).Evict(ctx, eviction)
		if err != nil {
			if strings.HasPrefix(err.Error(), "Cannot evict pod as it would violate") {
				log.Println("[INFO]", "honoring the", pods.Items[i].ObjectMeta.Name, "disruption budget")
				podsWithPolicy = append(podsWithPolicy, eviction)
			} else {
				log.Println("[ERROR]", err)
			}
		}
	}
	waitForThoseWithPolicy(ctx, k8s, podsWithPolicy)
}

// Testing every `drainWait` seconds if we can now delete the pod
// Waiting for a max of `drainMaxWait` seconds before we conclude - done.
// Deadlocks are possible with eviction policies
func waitForThoseWithPolicy(ctx context.Context, k8s *kubernetes.Clientset, podsWithPolicy []*policy.Eviction) {
	doneCh := make(chan bool)
	go func(ctx context.Context, k8s *kubernetes.Clientset, podsWithPolicy []*policy.Eviction) {
		for {
			time.Sleep(drainWait) // since the function is run even if the list is empty, this is a wait per node
			var stillpodsWithPolicy []*policy.Eviction
			for _, eviction := range podsWithPolicy {
				log.Println("[INFO]", "pod to evict", eviction.ObjectMeta.Name, "in", eviction.ObjectMeta.Namespace)
				err := k8s.PolicyV1beta1().Evictions(eviction.ObjectMeta.Namespace).Evict(ctx, eviction)
				if err != nil {
					log.Println("[INFO]", "waiting cause:", err)
					if strings.HasPrefix(err.Error(), "Cannot evict pod as it would violate") {
						log.Println("[DEBUG]", "adding", eviction.ObjectMeta.Name, "to the wait queue")
						stillpodsWithPolicy = append(stillpodsWithPolicy, eviction)
					}
				}
			}
			if len(stillpodsWithPolicy) == 0 {
				doneCh <- true
			}
			podsWithPolicy = stillpodsWithPolicy
		}
	}(ctx, k8s, podsWithPolicy)

	select {
	case <-doneCh:
		log.Println("[INFO]", "no more waiting pods, everything evicted")
		return
	case <-time.After(drainMaxWait):
		log.Println("[ERROR]", "drain did not complete within", drainMaxWait)
		if os.Getenv("handlewithcare") == envtrue {
			log.Println("[ERROR]", "as draining appears impossible, time to abort")
			os.Exit(1)
		}
		// if we don't handlewithcare we would terminate the instance
		return
	}
}

// k8s boilerplate - creating a client as single command
func k8sclient() *kubernetes.Clientset {
	if singleClient != nil {
		return singleClient
	}

	var config *rest.Config
	if os.Getenv("KUBECONFIG") == "" {
		rconfig, err := rest.InClusterConfig()
		must(err)
		config = rconfig
	} else {
		cconfig, err := clientcmd.BuildConfigFromFlags("", os.Getenv("KUBECONFIG"))
		must(err)
		config = cconfig
	}

	clientset, err := kubernetes.NewForConfig(config)
	if err != nil {
		log.Fatalln(err)
	}
	singleClient = clientset
	return clientset
}

// patch struct used to make nodes unschedulable
type patchBoolValue struct {
	Op    string `json:"op"`
	Path  string `json:"path"`
	Value bool   `json:"value"`
}
