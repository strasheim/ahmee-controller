package main

import (
	"log"
	"os"
	"strconv"
	"time"
)

const maxUpgradeDelay = 21
const fullday = 23
const secondsInADay = 86400 * time.Second

// time to upgrade is not a concrete time, this function is not even trying to be smart
// leap days / years ... nothing is accounted for.
// since upgrades can and will be re-run on the same day if the ahmee wasn't upgraded yet
// this function at best points to the start time of the upgrade run
func timeToUpgrade() bool {
	day := os.Getenv("upgradeday")
	delay := os.Getenv("upgradedelay")
	hour := os.Getenv("upgradehour")
	if day == "never" {
		return false
	}
	if day == "now" {
		return true
	}

	delayInt, err := strconv.Atoi(delay)
	must(err)
	if delayInt < 0 || delayInt > maxUpgradeDelay {
		log.Fatalln("[ERROR]", "upgrade delay cannot be greater than", maxUpgradeDelay, "days")
	}
	if delayInt > 0 {
		timedelay := time.Duration(delayInt)
		creationTime := availableAge(nextAMI(getCurrentK8sVersion()))
		upgradedelay := creationTime.Add(timedelay * secondsInADay)
		if !(upgradedelay.Before(time.Now())) {
			log.Println("[DEBUG]", "the Ahmee isn't old enough yet")
			return false
		}
	}

	userday := map[string]int{"Sunday": 0, "Monday": 1, "Tuesday": 2, "Wednesday": 3, "Thursday": 4, "Friday": 5, "Saturday": 6}
	if !(day == "any" || int(time.Now().Weekday()) == userday[day]) {
		log.Println("[DEBUG]", "the day of the upgrade is not today")
		return false
	}

	hourInt, err := strconv.Atoi(hour)
	must(err)
	if hourInt < 0 || hourInt > fullday {
		log.Fatalln("[ERROR]", "upgrade our needs to be between 0 and 23")
	}
	if time.Now().Hour() >= hourInt {
		return true
	}
	log.Println("[DEBUG]", "the sun isn't high enough yet - waiting")
	return false
}
