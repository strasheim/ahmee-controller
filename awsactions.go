package main

import (
	"errors"
	"log"
	"os"
	"sort"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/autoscaling"
	"github.com/aws/aws-sdk-go/service/ec2"
	"github.com/aws/aws-sdk-go/service/sts"
)

// To know that inside the container we got the correct role and arn.
// This does not changes once started and therefore is a nice startup print
func checkAWSconnection() {
	log.Println("[INFO]", "controller found in AWS region:", os.Getenv("AWS_DEFAULT_REGION"))
	sess := session.Must(session.NewSession(&aws.Config{Region: aws.String(os.Getenv("AWS_DEFAULT_REGION"))}))
	svc := sts.New(sess)
	input := &sts.GetCallerIdentityInput{}
	result, err := svc.GetCallerIdentity(input)
	must(err)
	log.Println("[INFO]", "using AWS arn:", *result.Arn)
}

// findASGs -- creates to maps for ID to LaunchT and ID to ASG mapping.
// this should reduce the inbound instances to only a couple of ASG and their Launchtemplates
func findASGs(instances []*string) (templates, asgnames map[string]string) {
	sess := session.Must(session.NewSession(&aws.Config{Region: aws.String(os.Getenv("AWS_DEFAULT_REGION"))}))
	svc := autoscaling.New(sess)

	input := &autoscaling.DescribeAutoScalingInstancesInput{
		InstanceIds: instances,
	}

	result, err := svc.DescribeAutoScalingInstances(input)
	must(err)
	templates = make(map[string]string)
	asgnames = make(map[string]string)
	for _, v := range result.AutoScalingInstances {
		templates[*v.LaunchTemplate.LaunchTemplateId] = *v.LaunchTemplate.Version
		asgnames[*v.LaunchTemplate.LaunchTemplateId] = *v.AutoScalingGroupName
	}
	return templates, asgnames
}

// asgByInstance is small maybe expensive helper to lookup the ASG name of the instance.
// Most ASG API interaction require the ASG name + the instance
func asgByInstance(instance string) string {
	sess := session.Must(session.NewSession(&aws.Config{Region: aws.String(os.Getenv("AWS_DEFAULT_REGION"))}))
	svc := autoscaling.New(sess)
	input := &autoscaling.DescribeAutoScalingInstancesInput{
		InstanceIds: []*string{
			aws.String(instance),
		},
	}
	result, err := svc.DescribeAutoScalingInstances(input)
	must(err)
	for _, v := range result.AutoScalingInstances {
		return *v.AutoScalingGroupName
	}
	return ""
}

// detach an Instance from its ASG, set to create a new spawn as the capacity isn't adjusted
// detaching WILL cause instances to no longer have ingress traffic
// Since our ASGs are per subnet, the respawn will be in the same location
func detachInstance(instance string) error {
	asgName := asgByInstance(instance)
	if asgName == "" {
		log.Println("[INFO]", "instance", instance, "was already detached")
		return errors.New("tick needed")
	}
	sess := session.Must(session.NewSession(&aws.Config{Region: aws.String(os.Getenv("AWS_DEFAULT_REGION"))}))
	svc := autoscaling.New(sess)
	input := &autoscaling.DetachInstancesInput{
		AutoScalingGroupName: aws.String(asgName),
		InstanceIds: []*string{
			aws.String(instance),
		},
		ShouldDecrementDesiredCapacity: aws.Bool(false),
	}
	_, err := svc.DetachInstances(input)
	if err == nil {
		log.Println("[INFO]", "instance", instance, "detached")
	}
	return err
}

// Wrapper for instance termination - it NOT inside an ASG
func terminateInstance(instance string) error {
	sess := session.Must(session.NewSession(&aws.Config{Region: aws.String(os.Getenv("AWS_DEFAULT_REGION"))}))
	svc := ec2.New(sess)
	input := &ec2.TerminateInstancesInput{
		InstanceIds: []*string{
			aws.String(instance),
		},
	}
	_, err := svc.TerminateInstances(input)
	return err
}

// Changing the ASG to use latest - regardless of if that has already been there
func setLatestToASG(asgUpdate map[string]string) {
	sess := session.Must(session.NewSession(&aws.Config{Region: aws.String(os.Getenv("AWS_DEFAULT_REGION"))}))
	svc := autoscaling.New(sess)

	for k, v := range asgUpdate {
		log.Println("[DEBUG]", "updating autoscalinggroup", v, k)
		input := &autoscaling.UpdateAutoScalingGroupInput{
			AutoScalingGroupName: aws.String(v),
			LaunchTemplate: &autoscaling.LaunchTemplateSpecification{
				LaunchTemplateId: aws.String(k),
				Version:          aws.String("$Latest"),
			},
		}
		_, err := svc.UpdateAutoScalingGroup(input)
		must(err)
	}
}

// upgradePossble checks if the ahmees in use have a next in line
func upgradePossible(list []*string) bool {
	if os.Getenv("revitalize") == envtrue {
		log.Println("[DEBUG]", "upgrading ahmees because revitalize is set")
		return true
	}
	if len(list) == 0 {
		log.Println("[INFO]", "no upgrade candidates found in the cluster")
		return false
	}

	latestAhmee := nextAMI(getCurrentK8sVersion())

	sess := session.Must(session.NewSession(&aws.Config{Region: aws.String(os.Getenv("AWS_DEFAULT_REGION"))}))
	svc := ec2.New(sess)
	input := &ec2.DescribeInstancesInput{InstanceIds: list}
	result, err := svc.DescribeInstances(input)
	must(err)
	for _, v := range result.Reservations {
		for _, inst := range v.Instances {
			if *inst.ImageId == latestAhmee {
				continue
			}
			return true
		}
	}
	return false
}

// This makes it possible to upgrade and reupgrade on failure
func onlyOldInstances(list worker) worker {
	if os.Getenv("revitalize") == envtrue {
		log.Println("[DEBUG]", "not reducing ahmee list because revitalize is set")
		return list
	}
	latestAhmee := nextAMI(getCurrentK8sVersion())
	sess := session.Must(session.NewSession(&aws.Config{Region: aws.String(os.Getenv("AWS_DEFAULT_REGION"))}))
	svc := ec2.New(sess)
	input := &ec2.DescribeInstancesInput{InstanceIds: list.awsslice()}
	result, err := svc.DescribeInstances(input)
	must(err)
	for _, v := range result.Reservations {
		for _, inst := range v.Instances {
			if *inst.ImageId != latestAhmee {
				continue
			}
			list.delete(*inst.InstanceId)
		}
	}
	return list
}

// This can be called like - availableAge(nextAMI(getCurrentK8sVersion()))
func availableAge(ahmee string) time.Time {
	sess := session.Must(session.NewSession(&aws.Config{Region: aws.String(os.Getenv("AWS_DEFAULT_REGION"))}))
	svc := ec2.New(sess)
	input := &ec2.DescribeImagesInput{
		ImageIds: []*string{
			aws.String(ahmee),
		},
	}
	result, err := svc.DescribeImages(input)
	must(err)
	if len(result.Images) != 1 {
		log.Fatalln("[ERROR]", "can't find the ago of the next image")
	}
	layout := "2006-01-02T15:04:05.000Z"
	gotime, err := time.Parse(layout, *result.Images[0].CreationDate)
	must(err)
	return gotime
}

// check what AMI id would be the next to install
// This check respects the current cluster version - nextAMI(getCurrentK8sVersion())
func nextAMI(clusterVersion string) string {
	sess := session.Must(session.NewSession(&aws.Config{Region: aws.String(os.Getenv("AWS_DEFAULT_REGION"))}))
	svc := ec2.New(sess)
	input := &ec2.DescribeImagesInput{
		Filters: []*ec2.Filter{
			{
				Name:   aws.String("name"),
				Values: []*string{aws.String("amazonlinux2-eks-worker-" + clusterVersion + "-" + "*")},
			},
			{
				Name:   aws.String("state"),
				Values: []*string{aws.String("available")},
			},
		},
	}
	result, err := svc.DescribeImages(input)
	must(err)

	if len(result.Images) != 0 {
		latest := mostRecentAmi(result.Images)
		return *latest.ImageId
	}
	log.Fatalln("no current image found matching the k8s clusterversion. Human intervention is needed")
	return ""
}

// This allows to sort for latest AMI
type imageSort []*ec2.Image

func (a imageSort) Len() int      { return len(a) }
func (a imageSort) Swap(i, j int) { a[i], a[j] = a[j], a[i] }
func (a imageSort) Less(i, j int) bool {
	itime, _ := time.Parse(time.RFC3339, *a[i].CreationDate)
	jtime, _ := time.Parse(time.RFC3339, *a[j].CreationDate)
	return itime.Unix() < jtime.Unix()
}

// Returns the most recent AMI out of a slice of images.
func mostRecentAmi(images []*ec2.Image) *ec2.Image {
	sortedImages := images
	sort.Sort(imageSort(sortedImages))
	return sortedImages[len(sortedImages)-1]
}

// create a new launchtemplate based on the version before and update the AMI only
func updateLaunchtemplate(ami string, template map[string]string) {
	sess := session.Must(session.NewSession(&aws.Config{Region: aws.String(os.Getenv("AWS_DEFAULT_REGION"))}))
	svc := ec2.New(sess)
	for k, v := range template {
		log.Println("[DEBUG]", "changing launchtemplate", k)
		currentTime := time.Now()
		input := &ec2.CreateLaunchTemplateVersionInput{
			LaunchTemplateData: &ec2.RequestLaunchTemplateData{
				ImageId: aws.String(ami),
			},
			LaunchTemplateId:   aws.String(k),
			SourceVersion:      aws.String(v),
			VersionDescription: aws.String("AhmeeController " + currentTime.Format("2006-01-02 15:04:05")),
		}
		_, err := svc.CreateLaunchTemplateVersion(input)
		must(err)
	}
}
