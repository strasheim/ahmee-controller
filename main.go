package main

import (
	"log"
	"time"
)

const waitingForNewAMI = 30 * time.Minute
const maxDrainGlobalWaitPerNode = 5 * time.Minute

func main() {
	setLogging()
	checkAWSconnection()
	handleSignal()

	// infinite loop -- waiting for the right time to change the ahmees
	for {
		eksworker := findInstanceInK8s()
		if !(upgradePossible(eksworker.awsslice()) && timeToUpgrade()) {
			log.Println("[INFO]", "no action required, sleeping for 30 minutes")
			time.Sleep(waitingForNewAMI)
			continue
		}

		log.Println("[INFO]", "upgrading the nodegroups")

		templates, asgnames := findASGs(eksworker.awsslice())
		setLatestToASG(asgnames)
		updateLaunchtemplate(nextAMI(getCurrentK8sVersion()), templates)

		// Changing the nodegroups before makes all new instance be already of the new ahmee
		done := make(chan bool, 1)
		go detachDrainTerminateNodes(done, eksworker)

		select {
		case <-done:
			log.Println("[DEBUG]", "finished in time")
		case <-time.After(maxDrainGlobalWaitPerNode * time.Duration(len(eksworker.k8sslice()))):
			log.Println("[ERROR]", "stopped processing waited for", maxDrainGlobalWaitPerNode*time.Duration(len(eksworker.k8sslice())))
		}
		log.Println("[INFO]", "pausing for 30 minutes for the cluster to settle, likely interrupted by instance termination")
		time.Sleep(waitingForNewAMI)
	}
}
