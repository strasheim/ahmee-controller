#!/bin/sh -e

container="<your-org>/ahmee-controller"
ecr_repo="<your-ecr>"
tag=$(git tag | tail -n 1)

docker build -t ${container} .

docker tag ${container}:latest ${ecr_repo}/${container}:${tag}
docker push ${ecr_repo}/${container}:${tag}
