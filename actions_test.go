package main

import (
	"fmt"
	"testing"

	"gotest.tools/assert"
)

func TestWorkerDelete(t *testing.T) {
	var myList worker

	host1 := eksglue{"aaabbbccc", "mumber1"}
	host2 := eksglue{"bbbcccddd", "mumber2"}
	host3 := eksglue{"cccdddeee", "mumber3"}
	myList.items = append(myList.items, host1, host2, host3)

	assert.Equal(t, fmt.Sprint(myList.k8sslice()), "[mumber1 mumber2 mumber3]", "they should be equal")
	myList.delete("bbbcccddd")
	assert.Equal(t, fmt.Sprint(myList.k8sslice()), "[mumber1 mumber3]", "they should be equal")
	myList.delete("cccdddeee")
	assert.Equal(t, fmt.Sprint(myList.k8sslice()), "[mumber1]", "they should be equal")
}
