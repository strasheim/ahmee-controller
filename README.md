### k8s ahmee controller

This controller replaces the AMI id in the ASGs from the cluster it is running it. Once the AMI ID has been updated it will detach the instance. This will cause the ASG to spawn new nodes with the new AMI. 

If the cluster was created via eksctl it should work out as long as your AMIs are decendent of the amazon linux 2 AMI. 

Once a new AMI is found the controller checks the upgrade schedule:
* UPGRADEDAY
* UPGRADEDELAY
* UPGRADEHOUR

Those 3 ENVs set the time when AMI upgrade will be applied:

Upgrade delay set the number of days between AMI becoming available and the next upgrade day. This allows dev/stage/prod cluster to change AMIs on different days, weeks. 
The max number that is allowed is 21 days. As only the last AMI is picked by the controller if a new AMI is published within the timeframe the clock is basically reset. 

The upgrade hour allows to control for offhours upgrades, it has to be a number between 0 and 23.

The upgrade day is a weekday OR **never**, **any**, **now** 
The weekdays are as you think. The **never** disables the controller, while you still get some log information. The **any** means it only waits for the delay and the hour and **now** means disregard all other parameters for upgrade time/day selection. 

### Logging options 

The loglevel has values:
* DEBUG
* INFO
* ERROR 
and can be set via the ENV loglevel

### AMI name 

As AMI sharing does not copy AMI tags the logic is based completely on the name of the AMI. The name is hardcoded in the code to 

```
amazonlinux2-eks-worker-" + clusterVersion + "-" + "*"
```

Meaning it has to start with amazonlinux2-eks-worker followed by a - on both sides of the k8s version the worker is for followed by anything you like.

### Instance exclusion 

You can label nodes inside the k8s cluster to not be picked by the ahmee-controller. Adding the label *ahmee-controller = ignore* will make those instance not subject ot termination or detachment. 


### Force Upgrade or ...

With env **handlewithcare=true** the wait for pod draining will terminate if draining can't be realized because of pod disruption budgets. This is false by default. The maxdrain wait is 4 minutes. 


### Triggering Upgrades 

For testing or error recovery it might useful to set **revitalize=true**, this will re-pick the existing AMI and cycle through a full upgrade.

### Logic flow 

Every 30 minutes the controller wakes up. Checks the time instances that are around. Here the exclusion would take place. The check is inside the k8s cluster not the ASGs. 
Next is checks if the time is an upgrade time and if a new (yet old enough) AMI is present to upgrade to. If no, go back to sleep. 
If conditions are okay, it will change the launchtemplates of the ASGs of instances inside the cluster. (scaling from 0 at 0 are missed on this)
Next it will detach an instance from the ASG, without changing the size, which will cause a new spawn from that ASG. If the ASGs are per subnet, this will give back a node identical to the just detached one, in the right place from the new AMI. Once a new ready node is noticed by the ahmee-controller, it will start draining the detached node. 
This cycle will repeat till the list it made early is worked thou. The list is ordered that the node where the ahmee-controller is on, is detached and terminated last. This is not needed but makes it easier to follow.  

