package main

import (
	"log"
	"os"
	"os/signal"
	"strings"
	"syscall"

	"github.com/hashicorp/logutils"
)

// must logs error on error and exits
func must(err error) {
	if err != nil {
		log.Fatalln("[ERROR]", err)
	}
}

// inSlice checks if a given string is in the slice
func inSlice(slice []string, needle string) bool {
	for _, item := range slice {
		if item == needle {
			return true
		}
	}
	return false
}

// setLogging, wrappper for setting logging, defaults to INFO
func setLogging() {
	logging := "INFO"
	if os.Getenv("loglevel") != "" {
		logging = strings.ToUpper(os.Getenv("loglevel"))
	}
	filter := &logutils.LevelFilter{
		Levels:   []logutils.LogLevel{"DEBUG", "INFO", "ERROR"},
		MinLevel: logutils.LogLevel(logging),
		Writer:   os.Stdout,
	}
	log.SetOutput(filter)
	log.SetFlags(log.Ltime)
	log.Println("[INFO]", "controller located on", os.Getenv("instancename"))
}

// Adding signal handling to the container
func handleSignal() {
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM)
	go func() {
		<-sigs
		log.Println("[INFO]", "terminiation wish received, bye bye")
		os.Exit(0)
	}()
	log.Println("[DEBUG]", "sigint and sigterm handler installed")
}
